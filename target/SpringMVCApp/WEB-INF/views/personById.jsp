<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form" %>
    <%@ page isELIgnored="false"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
		<h2>Search Student By Id</h2>
		<form:form method="POST" action ="/SpringMVCApplic/idperson" modelAttribute="personId">
			<table>
				<tr>
					<td> <form:label path = "id">Id: </form:label></td>
					<td> <form:input path = "id"></form:input></td>
				</tr>
				<tr>
					<td colspan= "2">
						<input type = "submit" value = "Submit"/>
					</td>
				</tr>
			</table>
		</form:form>
</body>
</html>