package com.softtek.academy.javaweb.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.dao.PersonDao;

@Repository("personRepository")
public class PersonRepository implements PersonDao {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public int save(Person e) {
		System.out.println("ENTRO AL SAVE"+e.getName());
		Person person = new Person(e.getId(),e.getName(),e.getAge());
		/*person.setAge(e.getAge());
		person.setId(e.getId());
		person.setName(e.getName());*/
		entityManager.persist(person);
		return 0;

	}

	@Override
	public List<Person> getPersons() {
		System.out.println("ENTRA AL REPOSITORY getPersons");
		return (List<Person>)entityManager.createQuery("Select c from Person c", Person.class).getResultList();
	}
	
	@Override
	public int deletePerson(long id) {
		entityManager.remove(entityManager.find(Person.class, id));		
		return 0;
	}
	
	@Override
	public Person getById(long id) {
		return entityManager.find(Person.class,id);
	}

	@Override
	public int updatePerson(Person s) {
		Person actual = entityManager.find(Person.class, s.getId());
		actual.setAge(s.getAge());
		actual.setName(s.getName());
		entityManager.merge(actual);
		return 0;
	}



}
