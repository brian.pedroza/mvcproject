package com.softtek.academy.javaweb.services;

import java.util.List;

import javax.persistence.Query;

import com.softtek.academy.javaweb.beans.Person;

public interface PersonService {
	int save(Person e);
	
	List<Person> getPersons();

	int deletePerson(long id);

	Person getById(long id);

	int updatePerson(Person s);
}
