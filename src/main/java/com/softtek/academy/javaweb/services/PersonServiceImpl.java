package com.softtek.academy.javaweb.services;

import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.dao.PersonDao;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {
	
	@Qualifier("personRepository")
	private PersonDao personDao;
	
	public PersonServiceImpl(PersonDao personDao) {
		this.personDao = personDao;
	}

	@Override
	public int save(Person e) {
		// TODO Auto-generated method stub
		return personDao.save(e);
	}

	@Override
	public List<Person> getPersons() {
		System.out.println("ENTRA AL SERVICE");
		return personDao.getPersons();
	}
	
	@Override
	public int deletePerson(long id) {
		// TODO Auto-generated method stub
		return personDao.deletePerson(id);
	}
	
	@Override
	public Person getById(long id) {
		return personDao.getById(id);
	}

	@Override
	public int updatePerson(Person s) {
		return personDao.updatePerson(s);
	}


}
