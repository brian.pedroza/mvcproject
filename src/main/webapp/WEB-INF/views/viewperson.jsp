<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ page isELIgnored="false"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
TODOS LOS REGISTROS
	<table border="2" width="70%" cellpadding="2">
         <tr>
            <th>Id</th>
            <th>Age</th>
            <th>Name</th>
         </tr>
          <c:forEach items="${list}" var="v">
              <tr>
              	  <td>${v.id}</td>
                  <td>${v.age}</td>
                  <td>${v.name}</td> 
                  <form:form method="POST" action ="/SpringMVCApplic/personupdate" modelAttribute="personUP">
                    <form:input path = "id" value="${v.id}" hidden="true"></form:input>
                  	<td><input type = "submit" value = "Change"/></td>
                  </form:form>
               </tr>
          </c:forEach>
     </table> 

</body>
</html>