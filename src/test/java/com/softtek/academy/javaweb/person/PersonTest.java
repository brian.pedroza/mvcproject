package com.softtek.academy.javaweb.person;

import static org.junit.Assert.assertSame;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.javaweb.configuration.JPAConfiguration;
import com.softtek.academy.javaweb.repositories.PersonRepository;
import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.services.PersonService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfiguration.class })
@WebAppConfiguration
public class PersonTest {

	@Autowired
	private PersonService personService;
	@Autowired
	private PersonRepository repo;
	
	@Test
	public void getPersonsTestUnit() {
		List<Person> persons= repo.getPersons();
		long actual = persons.get(0).getId();
		long expected = 3;
		assertSame(expected, actual);
	}
	
	@Test
	public void getPersonsTest() {
		List<Person> persons = personService.getPersons();
		long actual = persons.get(0).getId();
		long expected = 3;
		assertSame(expected, actual);
	}

}

